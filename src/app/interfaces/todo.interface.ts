export interface IToDo {
  id: string;
  title: string;
  done: boolean;
  id_user: number;
}
