import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgModule} from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

import {TodoListService} from './services/todo-list.service';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HeaderComponent} from './components/header/header.component';
import {TodoListItemsComponent} from './components/todo-list-items/todo-list-items.component';
import {TodoListItemComponent} from './components/todo-list-item/todo-list-item.component';
import {AddTodoFormComponent} from './components/add-todo-form/add-todo-form.component';
import {EditTodoItemModalComponent} from './components/edit-todo-item-modal/edit-todo-item-modal.component';
import { StartupService } from './services/startup.service';

import { AuthHttpInterceptor } from './services/auth-http.interceptor';

import {ToastaModule} from 'ngx-toasta';
import {RegisterComponent} from './components/register/register.component';
import {TopMenuComponent} from './components/top-menu/top-menu.component';
import {LoginComponent} from './components/login/login.component';
import {HomeComponent} from './components/home/home.component';
import {AuthService} from './services/auth.service';
import {AuthGuardService} from './services/auth-guard.service';
import {JwtModule} from '@auth0/angular-jwt';
import {NgxSpinnerModule} from 'ngx-spinner';
import {AddToastaService} from './services/add-toasta.service';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    TodoListItemsComponent,
    TodoListItemComponent,
    AddTodoFormComponent,
    EditTodoItemModalComponent,
    RegisterComponent,
    TopMenuComponent,
    LoginComponent,
    HomeComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule.forRoot(),
    ToastaModule.forRoot(),
    JwtModule.forRoot({
      config: {
        tokenGetter: () => {
          return localStorage.getItem('token');
        },
      }
    }),
    NgxSpinnerModule
  ],
  exports: [
    BrowserModule,
    ToastaModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthHttpInterceptor,
      multi: true
    },
    TodoListService,
    NgbActiveModal,
    AuthService,
    AuthGuardService,
    AddToastaService,
    StartupService,
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    EditTodoItemModalComponent
  ],
})
export class AppModule {
}
