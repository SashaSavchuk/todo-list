import { Component, OnInit } from '@angular/core';
import { StartupService } from './services/startup.service';
import { AuthService } from './services/auth.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'todo-list';
  constructor(private startup: StartupService, public auth: AuthService) {
  }
  ngOnInit() {
    this.startup.loadApp();
  }
  get user() {
    return this.auth.user;
  }
  get appLoaded() {
    return this.startup.appLoaded;
  }
}
