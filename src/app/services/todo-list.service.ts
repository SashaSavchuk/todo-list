import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ToastaService, ToastaConfig, ToastOptions} from 'ngx-toasta';
import {IToDo} from '../interfaces/todo.interface';

@Injectable({
  providedIn: 'root'
})
export class TodoListService {

  public dataList: IToDo[] = [];

  constructor(public http: HttpClient, private toastaService: ToastaService, private toastaConfig: ToastaConfig) {
    this.toastaConfig.theme = 'bootstrap';
  }

  getData() {
    this.http.get(`http://localhost:3003/`)
      .subscribe((items: IToDo[]) => {
        this.dataList = items;
      });
  }

  changeItemStatus(item) {
    this.http.put(`http://localhost:3003/${item.id}`, {done: !item.done})
      .subscribe(
        () => {
          const todo = this.dataList.find(data => data.id === item.id);
          if (todo) {
            todo.done = !item.done;
          } else {
            this.addToastError();
          }
        },
        () => {
          this.addToastError();
        }
      );
  }

  editItem(id: string, title: string) {
    this.http.put(`http://localhost:3003/${id}`, {title})
      .subscribe(
        () => {
          const todo = this.dataList.find(item => item.id === id);
          if (todo) {
            todo.title = title;
          } else {
            this.addToastError();
          }
        },
        () => {
          this.addToastError();
        }
      );
  }

  deleteItem(id: string) {
    this.http.delete(`http://localhost:3003/${id}`)
      .subscribe(
        () => {
          const itemIndexDelete = this.dataList.findIndex(item => item.id === id);
          this.dataList.splice(itemIndexDelete, 1);
        },
        () => {
          this.addToastError();
        }
      );
  }

  createItem(title: string) {
    this.http.post('http://localhost:3003/', {'title': title})
      .subscribe(
        data => {
          this.dataList.push(<IToDo>data);
        },
        () => {
          this.addToastError();
        });
  }

  addToastError() {
    const toastOptions: ToastOptions = {
      title: 'Something went wrong',
      msg: 'Please inform the administrator',
      showClose: true,
      timeout: 5000,
    };
    this.toastaService.error(toastOptions);
  }
}
