import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {IUser} from '../interfaces/user.interface';
import {NgxSpinnerService} from 'ngx-spinner';
import {Router} from '@angular/router';
import {AddToastaService} from './add-toasta.service';
import {AppConstants} from '../config';
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  base_url: string;

  constructor(private http: HttpClient, private spinner: NgxSpinnerService, public router: Router, public addToast: AddToastaService) {
    this.base_url = AppConstants.baseURL;
  }

  public user: Object;

  get isLoggedIn() {
    return this.isAuthenticated();
  }

  public isAuthenticated(): boolean {
    return typeof this.user !== 'undefined' && Object.keys(this.user).length > 0;
  }

  setUser(user: any): any {
    return this.user = user;
  }

  checkUserAuth(): Observable<any> {
    return this.http.get(this.base_url + 'user', {headers: {Authorization: `Bearer ${localStorage.getItem('token')}`}});
  }

  createUser(user: IUser) {
    this.spinner.show();
    this.http.post(this.base_url + 'auth/register', user)
      .subscribe(
        (res: any) => {
          this.addToast.addToast('Congratulations you are registered', '', 'success');
          this.loginUser(user.email, user.password, 'new');
          this.spinner.hide();
        },
        () => {
          this.addToast.addToast('Something went wrong', 'Please inform the administrator', 'error');
          this.spinner.hide();
        });
  }

  loginUser(email: string, password: string, status) {
    this.spinner.show();
    return this.http.post(this.base_url + 'auth/login', {email, password})
      .subscribe(
        (data: { token: string }) => {
          const token = data.token;
          if (token) {
            localStorage.setItem('token', token);
            this.checkUserAuth()
              .subscribe((user) => {
                this.setUser(user);
              });
            if (status === 'odl') {
              this.addToast.addToast('Congratulations you are login', '', 'success');
              this.spinner.hide();
              this.router.navigateByUrl('/list-items');
            } else {
              this.spinner.hide();
              this.router.navigateByUrl('/');
            }
          } else {
            this.addToast.addToast('Something went wrong', 'Please inform the administrator', 'error');
          }
        },
        (error) => {
          this.addToast.addToast('Something went wrong', 'Please inform the administrator', 'error');
          this.spinner.hide();
        });
  }

  logout(): void {
    this.user = {};
    localStorage.removeItem('token');
  }
}
