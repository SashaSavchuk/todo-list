import { Injectable } from '@angular/core';
import {ToastaConfig, ToastaService, ToastOptions} from 'ngx-toasta';

@Injectable({
  providedIn: 'root'
})
export class AddToastaService {

  constructor(private toastaService: ToastaService, private toastaConfig: ToastaConfig) {
    this.toastaConfig.theme = 'bootstrap';
  }

  addToast(title: string, msg: string = '', status: string) {
    const toastOptions: ToastOptions = {
      title: title,
      msg: msg,
      showClose: true,
      timeout: 5000,
    };
    switch (status) {
      case ('success'):
        this.toastaService.success(toastOptions);
        break;
      case ('error'):
        this.toastaService.error(toastOptions);
        break;
      default:
        break;
    }
  }
}
