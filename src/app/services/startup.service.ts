import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { AddToastaService } from './add-toasta.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Injectable({
  providedIn: 'root'
})
export class StartupService {

  public user: Object;
  public appLoaded = false;
  constructor(public authService: AuthService, private spinner: NgxSpinnerService, public router: Router) { }

  loadApp(): any {
    this.authService.checkUserAuth()
    .subscribe(
      (user) => {
        this.appLoaded = true;
        this.spinner.hide();
        this.user = this.authService.setUser(user);
      },
      (error) => {
        this.appLoaded = true;
        this.spinner.hide();
      });
  }
}
