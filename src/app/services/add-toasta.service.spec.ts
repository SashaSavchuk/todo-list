import { TestBed } from '@angular/core/testing';

import { AddToastaService } from './add-toasta.service';

describe('AddToastaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AddToastaService = TestBed.get(AddToastaService);
    expect(service).toBeTruthy();
  });
});
