import { Component, OnInit, Input } from '@angular/core';
import { TodoListService } from '../../services/todo-list.service';
import { EditTodoItemModalComponent } from '../edit-todo-item-modal/edit-todo-item-modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { IToDo } from '../../interfaces/todo.interface';


@Component({
  selector: 'app-item',
  templateUrl: './todo-list-item.component.html',
  styleUrls: ['./todo-list-item.component.scss']
})
export class TodoListItemComponent implements OnInit {

  @Input() item;

  constructor(private data: TodoListService, private modalService: NgbModal) { }

  ngOnInit() {
  }

  changeStatus(item: IToDo): void {
    this.data.changeItemStatus(item);
  }

  deleteItem(id: string) {
    if (confirm('Delete this item?')) {
      this.data.deleteItem(id);
    }
  }

  openFormModal() {
    const modalRef = this.modalService.open(EditTodoItemModalComponent);
    modalRef.componentInstance.id = this.item.id;
    modalRef.componentInstance.title = this.item.title;
    modalRef.result.then(result => {
    }).catch(error => {
    });
  }
}
