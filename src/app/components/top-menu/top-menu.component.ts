import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-top-menu',
  templateUrl: './top-menu.component.html',
  styleUrls: ['./top-menu.component.scss']
})
export class TopMenuComponent implements OnInit {

  constructor(private authService: AuthService) {}

  ngOnInit() {
  }

  get user() {
    return this.authService.user;
  }

  get isLoggedIn() {
    return this.authService.isAuthenticated();
  }
}
