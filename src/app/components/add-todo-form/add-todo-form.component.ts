import { Component, OnInit } from '@angular/core';
import { TodoListService } from '../../services/todo-list.service';

@Component({
  selector: 'app-add-form',
  templateUrl: './add-todo-form.component.html',
  styleUrls: ['./add-todo-form.component.scss']
})
export class AddTodoFormComponent implements OnInit {

  title: String;

  constructor(private data: TodoListService) { }

  ngOnInit() {
  }

  addItem(title) {
      this.data.createItem(title);
      this.title = '';
  }
}
