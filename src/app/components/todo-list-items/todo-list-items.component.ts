import { Component, OnInit } from '@angular/core';
import { TodoListService } from '../../services/todo-list.service';

@Component({
  selector: 'app-list-item',
  templateUrl: './todo-list-items.component.html',
  styleUrls: ['./todo-list-items.component.scss']
})
export class TodoListItemsComponent implements OnInit {

  constructor(private listService: TodoListService) { }

  ngOnInit() {
    this.listService.getData();
  }

  get items() {
    return this.listService.dataList;
  }

}
