import {Component, Input, OnInit} from '@angular/core';
import { TodoListService } from '../../services/todo-list.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-edit-modal',
  templateUrl: './edit-todo-item-modal.component.html',
  styleUrls: ['./edit-todo-item-modal.component.scss']
})
export class EditTodoItemModalComponent implements OnInit {
  @Input() id;
  private editTodoItem: FormGroup;
  constructor(private data: TodoListService, public activeModal: NgbActiveModal) { }

  ngOnInit() {
    this.editTodoItem = new FormGroup({
      title: new FormControl()
    });
  }
  saveChanges() {
    this.data.editItem(this.id, this.editTodoItem.value.title);
    this.activeModal.dismiss('Close clicked');
  }
}
