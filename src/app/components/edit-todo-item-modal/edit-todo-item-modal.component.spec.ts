import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditTodoItemModalComponent } from './edit-todo-item-modal.component';

describe('EditModalComponent', () => {
  let component: EditTodoItemModalComponent;
  let fixture: ComponentFixture<EditTodoItemModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditTodoItemModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditTodoItemModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
